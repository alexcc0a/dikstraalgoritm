package com.nesterov;

import java.util.Arrays;

public class DikstraAlgoritm {
    //создаем константу INFINITY и присваеваем ей максимальное значение типа Integer
    private static final int INFINITY = Integer.MAX_VALUE;

    //создаем метод dikstra, который принимает на вход массив graph и целочисленное значение start
    public static void dikstra(int[][] graph, int start) {
        //Получаем колличество ваершин в графе
        int vertices = graph.length;
        //Создаем массив dist размером visited, который будет хранить расстояние от начальной вершины до каждой вершины графа
        int[] dist  = new int[vertices];
        //Создаем массив visited размером vertices, который будет хранить информацию о посещении каждой вершины графа
        boolean[] visited = new boolean[vertices];

        //Заполнияем массив dist значением INFINITY для всех вершин, кроме начальной, для которой расстоние равно 0
        Arrays.fill(dist, INFINITY);
        dist[start] = 0;

        //Выполняем цикл от 0 до vertices-1, который будет итерироваться по всем вершинам графа, кроме начальной
        for (int i = 0; i < vertices - 1; i++) {
            //Находим вершину с минимальным расстоянием из массива dist, которая еще не была посещена
            int u = findMinDistance(dist, visited);
            visited[u] = true;
            //Выполняем цикл по всем вершинам графа
            for (int v = 0; v < vertices; v++) {
                //Если вершина еще не была посещенаБ между вершинами u и v есть ребро, расстояние до вершины u не равно INFINITY,
                //и расстояние от начальной вершины до вершины u плюс вес ребра до вершины v меньше текущего расстояния до вершины v,
                //то обновляем значение dist[v]
                if (!visited[v] && graph[u][v] != 0 && dist[u] != INFINITY &&
                        dist[u] + graph[u][v] < dist[v]) {
                    dist[v] = dist[u] + graph[u][v];
                }
            }
        }
        //Выводим на экран расстояния от начальной вершины до всех остальных вершин графа
        printDistances(dist);
    }
    //Создаем метод findMinDistance, который принимает на вход массив dist и массив visited
    private static int findMinDistance(int[] dist, boolean[] visited) {
        //Инициализируем переменную minDistance значением INFINITY
        int minDistance = INFINITY;
        //Инициализируем переменную minIndex значением -1
        int minIndex = -1;

        //Выполняем цикл по всем элементам массива dist
        for (int v = 0; v < dist.length; v++) {
            //Если вершина v еще не была посещена и расстояние до нее меньше или равно текущему минимальному расстоянию,
            //то обновляем значения переменных minDistance и minIndex
            if (!visited[v] && dist[v] <= minDistance) {
                minDistance = dist[v];
                minIndex = v;
            }
        }

        //Возвращаем индекс вершины с минимальным расстоянием
        return minIndex;
    }
    //Создаем мотод printDistance, котоый принимает на вход массив dist
    // и выводит на экран расстояния от начальной вершины до всех остальных вершин графа
    private static void printDistances(int[] dist) {
        System.out.println("Virtex \t Distance from Start");
        for (int i = 0; i < dist.length; i++) {
            System.out.println(i + "\t\t" + dist[i]);
        }
    }

    //Создаем метод main, в котором задаем двумерный массив graph, вызываем метод dikstra и передаем ему начальную вршину
    public static void main(String[] args) {
        int[][] graph = {
                {0, 4, 0, 0, 0, 0, 0, 8, 0},
                {4, 0, 8, 0, 0, 0, 0, 11, 0},
                {0, 8, 0, 7, 0, 4, 0, 0, 2},
                {0, 0, 7, 0, 9, 14, 0, 0, 0},
                {0, 0, 0, 9, 0, 10, 0, 0, 0},
                {0, 0, 4, 14, 10, 0, 2, 0, 0},
                {0, 0, 0, 0, 0, 2, 0, 1, 6},
                {8, 11, 0, 0, 0, 0, 1, 0, 7},
                {0, 0, 2, 0, 0, 0, 6, 7, 0},
        };

        dikstra(graph, 0);
    }
}
